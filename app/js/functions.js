function urlExtension(CONFIGS){
    const url        = location.href;
    const regexUrl   = /(http:\/\/|https:\/\/)(.*)/i;
    const regexLocal = /(192\.168\.1|127\.0\.0|localhost)/i;

    // PRODUCTION
    if(!url.match(regexUrl)) return '/';

    let [complete, prefix, parcial, index, input] = url.match(regexUrl);
    let matchLocal                                = parcial.match(regexLocal);

    // LOCAL DEVELOPMENT
    if(matchLocal){
        let splitted_parcial = parcial.split('/');
        let joins = [];
        for(let splits of splitted_parcial){
            joins.push(splits);
            if(splits.match(/dist/)) break;
        }
        return prefix + joins.join('/') + '/';
    }
    // PRODUCTION FALLBACK
    if(CONFIGS.APPROVAL.active) return CONFIGS.COMPANY.URL.base;
    return '/';
}

function swap(arr, x, y){
    let b  = arr[x];
    arr[x] = arr[y];
    arr[y] = b;
    return arr;
}

function searchArray(arr, field, value){
    for(let index in arr){
        let val = arr[index];
        if(val[field] == value) return val;
    }
}

function searchArrayMultiple(arr, field, value){
    let result = [];
    for(let index in arr){
        let val = arr[index];
        if(val[field] == value) result.push(val);
    }
    return result;
}

function copy(o) {
    let output, v, key;
    output = Array.isArray(o) ? [] : {};
    for (key in o) {
        v = o[key];
        output[key] = (typeof v === "object") ? copy(v) : v;
    }
    return output;
}

function isLocalhost(url){
    const testRegex = /192\.168\.1|127\.0\.0|localhost/i;
    return url.match(testRegex) ? true : false;
}

function resolveStructure(arr, field){
    const structure = arr.filter(item => Number(item[field]) === 0);

    // sub level
    if(structure.length){
        structure.forEach(main => {
            main.items = searchArrayMultiple(arr, field, main.id);

            // last level
            if(main.items.length){
                main.items.forEach(item => {
                    item.items = searchArrayMultiple(arr, field, item.id);
                })
            }
        })
    }
    return structure;
}