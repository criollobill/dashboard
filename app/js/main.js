angular.module('app', [
    'app.filters',
	'ui.router',
	'ngSanitize',
	'ui.bootstrap',
	'ui.utils.masks',
	'monospaced.elastic',
	'html5.sortable',
])
.run(($rootScope, $document, $window, $state, $transitions, Post, CONFIGS)=>{
    $rootScope.CONFIGS = CONFIGS;
    $rootScope.scroll = {location: 0, blocked: false};
    $rootScope.current_page = '';

    // aside
    $rootScope.asideOpen = false;
    $rootScope.toggleAside = function(){
        $rootScope.asideOpen = !$rootScope.asideOpen;
        //block main scroll
        $rootScope.scroll.blocked = !$rootScope.scroll.blocked;
    };

    // logout method
    $rootScope.logout = function(){
        Post('login/logout').then(() => {
            location.reload();
        },
        err => {
            console.log("err", err);
        })
    };

    $document.on('scroll', function() {
        $rootScope.$apply(function() {
            $rootScope.scroll.location = $window.scrollY;
        })
    });

    $transitions.onStart({}, () => {
        $rootScope.asideOpen      = false;
        $rootScope.scroll.blocked = false;
    });

    $transitions.onSuccess({}, () => {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
        $rootScope.current_page   = $state.current.name.replace(/[.]/gi, '-');
	});
})
.config(($stateProvider, $urlRouterProvider, $locationProvider)=>{
 	isLocalhost(location.href) ? $locationProvider.html5Mode(false) : $locationProvider.html5Mode(true);
 	$urlRouterProvider.otherwise('/');

	$stateProvider
	.state('main', {
		templateUrl: 'views/structure/main.html',
	})	
	.state('main.panel', {
		url: '/',
		templateUrl: 'views/panel/panel.html',
        controller: 'cPanel',
        controllerAs: '$ctrl'
	})
	.state('main.404', {
		url: '/404',
		templateUrl:'views/error/404.html',
	})
})
//////////////////////////////////////////
// Controllers
//////////////////////////////////////////
.controller('cLogin', cLogin)
.controller('cAside', cAside)
.controller('cPanel', cPanel)

//////////////////////////////////////////
// Components
//////////////////////////////////////////
.component('pageHeader', PageHeader)
.component('uploadFiles', UploadFiles)
.component('pageLoading', PageLoading)

//////////////////////////////////////////
// Directives
//////////////////////////////////////////
.directive('dErrSrc', () => new errSrc())

//////////////////////////////////////////
// Factory
//////////////////////////////////////////
.factory('Post', Post)