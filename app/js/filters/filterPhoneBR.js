function filterPhoneBR(){
    return (phone) => {
        const formatter = new StringMask('00 9000 0000');
        return formatter.apply(phone);
    }
}