<?php
session_start();

include_once('php/__configs.php');

$http_host   = $_SERVER['HTTP_HOST'];
$request_uri = $_SERVER['REQUEST_URI'];
$regex_local = '/192\.168\.1|127\.0\.0|localhost/';

function fileInclude($file){
    $temp   = pathinfo($file);
    echo $file.'?cachetime=' . filemtime($temp['basename']);
}

function getBase(){
    global $http_host;
    global $request_uri;
    global $regex_local;

    $http = @$_SESSION['CONFIGS']['COMPANY']['URL']['ssl'] ? 'https://' : 'http://';
    $www  = substr($http_host, 0, 3) == 'www' ? 'www.' : '';

    if(preg_match($regex_local, $http_host)){
        $joins = [];
        $splitted_url = explode('/', $request_uri);
        foreach($splitted_url as $splits){
            $joins[] = $splits;
            if(preg_match('/dist/', $splits)) break;
        }
        return 'http://' . $http_host . implode('/', $joins) . '/';
    }else{
        if($_SESSION['CONFIGS']['APPROVAL']['active']){
            return $http . $http_host . str_replace('/adm', '', $request_uri);
        }
        return $http . $http_host .'/';
    }
}

if($_SESSION['CONFIGS']['COMPANY']['URL']['ssl'] && !@$_SERVER['HTTPS'] && !preg_match($regex_local, $http_host)){
    header( 'location: https://' . $http_host . $request_uri );
    die();
}
?>

<!DOCTYPE html>
<html ng-app="app">
<head>
<base href="<?php echo getBase(); ?>" />

<title>Painel Administrativo</title>

<meta content="description" name="Painel Administrativo">
<meta content="Keywords" name="keywords">

<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport">
<meta content="pt-br" http-equiv="content-language">

<meta content="noindex, nofollow" name="robots">
<meta content="noindex, nofollow" name="googlebot">

<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">

<!-- css -->
<link rel="stylesheet" type="text/css" href="<?php fileInclude('css.css'); ?>">
</head>
<body ng-class="{'scroll_blocked': scroll.blocked}">
<?php
if(!@$_SESSION['administrator_logged']){
    require_once('views/login/login.html');
}else{
?>
    <ui-view></ui-view>
<?php
}
?>
<script type="text/javascript" src="<?php fileInclude('libs.js'); ?>"></script>
<script type="text/javascript" src="<?php fileInclude('js.js'); ?>"></script>
<script type="text/javascript" src="<?php fileInclude('const.php'); ?>"></script>
</body>
</html>
