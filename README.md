# AngularJS Dashboard boilerplate

### Getting started
- make a clone of this repository `git clone https://recs182@bitbucket.org/recs182-boilerplates/dashboard.git project_name`
- delete .git `rm -rf .git`
- install packages `npm i`
- run gulp in a terminal
- see your project at `dist` folder, edit then in `app` folder